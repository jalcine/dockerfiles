# A Base Image to Deploy Python WSGI Apps with uWSGI

This image installs uWSGI, and adds a few settings for easy further deployment
of Python WSGI apps.

## Supported Tags

- [python2](https://gitlab.com/blowb/dockerfiles/blob/master/uwsgi/python2/Dockerfile)

## How to Use This Image

This image is used as a base image, which means that you should not run this
image directly, but create your own app image based on this image.

To create an app image based on this image, you should set either your
`WSGI_MODULE` variable to be the WSGI module of your app in your Dockerfile, or
`WSGI_FILE` to be the WSGI file. Furthermore, you should put all writable files
in the directory of `/var/uwsgi`, as this image runs uWSGI as a non-privileged
user, and `/var/uwsgi` is made to be writable by the uWSGI processes. You can
also set `ADDITIONAL_ARGUMENTS` to additional arguments you want to add when
starting the uWSGI process(es).

Please note that this image does not install a web server such as Nginx, which
should be configured in a separate Docker container or the host to communicate
with this Docker container instance. The port that uWSGI listens is hard coded
to be 9000.

This image also adds a list of environment variables that can be used when run
your app image derived from this image:

- `NUM_PROCESSES`

  Set this number to the number of processes you want to start.

- `NUM_THREADS`

  Set this number to the number of threads of each process.

- `ADITIONAL_USER_ARGUMENTS`

  This can be used to add more uWSGI startup options by the user who runs your
  app image.

To ensure the environment variables are used, please make sure
`/usr/local/bin/run.sh` is used to start the uWSGI process(es). This script
has been set to the default command to start the process(es), so for simple
cases you should not need to add a `CMD` command in your Dockerfile that
creates your app image.

You can enable additional uWSGI plugins by calling `docker-install-uwsgi-plugin
plugin_name`.

There are two examples [Mozilla sync server][] and [isso][] which use this image
as a base.

[Mozilla sync server]: https://gitlab.com/blowb/dockerfiles/blob/master/mozilla-sync-server/Dockerfile
[isso]: https://gitlab.com/blowb/dockerfiles/blob/master/isso/Dockerfile