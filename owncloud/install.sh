#!/bin/bash

set -e

cd /var/www/html

if [ -z "$(ls -A)" ]; then
  curl -L -s https://download.owncloud.org/community/owncloud-latest.tar.bz2 \
    | tar jxvf - --strip-components=1
fi
