#!/bin/bash

set -e

if [ -x /usr/local/bin/install.sh ]; then
  /usr/local/bin/install.sh
fi

chown -R www-data:www-data /var/www

exec apache2-foreground
