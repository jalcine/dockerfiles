#!/bin/bash

set -e

chown -R prosody.prosody /var/run/prosody

exec /usr/bin/prosodyctl start
